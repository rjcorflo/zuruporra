<?php
/**
 * Created by PhpStorm.
 * User: RJ Corchero
 * Date: 03/06/2016
 * Time: 20:51
 */

function zuruporra_admin_edit_form($form, &$form_state, $idJornada) {
  $form_state['id_jornada'] = $idJornada;

  $selectPartidosJornada = db_select('porra_partidos', 'p');
  $selectPartidosJornada
    ->fields('p', array('id_partido','fecha_partido', 'equipo_1', 'equipo_2', 'resultado_1', 'resultado_2'))
    ->condition('p.id_jornada', $idJornada, '=')
    ->orderBy('p.id_partido', 'ASC');
  $result = $selectPartidosJornada->execute();

  $form['field_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Listado de partidos: @jornada', array('@jornada' => recuperar_nombre_jornada($idJornada))),
    '#tree' => true,
    '#attributes' => array('class' => array('zuruporra'))
  );

  while ($resultado = $result->fetchAssoc()) {
    if (isset($resultado['fecha_partido'])) {
      $date = (new DateTime())->setTimestamp($resultado['fecha_partido']);
      $fechaHoy = new DateTime();
      $desplegado = $fechaHoy->getTimestamp() > $date->getTimestamp();
    } else {
      $date = new DateTime();
      $desplegado = true;
    }

    if (isset($resultado['equipo_1']) && isset($resultado['equipo_2'])) {
      $titulo = t('Partido: @equipo1 - @equipo2 (@fecha)',
        array(
          '@equipo1' => $resultado['equipo_1'],
          '@equipo2' => $resultado['equipo_2'],
          '@fecha' => $date->format('d-m-Y H:i')
          ));
    } else {
      $titulo = t('Partido vacío');
    }

    $form['field_group'][$resultado['id_partido']] = array(
      '#type' => 'fieldset',
      '#title' => $titulo,
      '#tree' => true,
      '#collapsible' => true,
      '#collapsed' => $desplegado
    );

    $dateFormat = $date->format('Y-m-d H:i:s');
    $format = 'd-m-Y H:i';

    $form['field_group'][$resultado['id_partido']]['fecha_partido'] = array(
      '#type' => 'date_select', // types 'date_popup', 'date_text' and 'date_timezone' are also supported. See .inc file.
      '#title' => t('Fecha partido'),
      '#default_value' => $dateFormat,
      '#date_format' => $format,
      '#date_increment' => 15, // Optional, used by the date_select and date_popup elements to increment minutes and seconds.
    );
    $form['field_group'][$resultado['id_partido']]['equipo_1'] = array(
      '#type' => 'textfield',
      '#title' => 'Equipo 1',
      '#default_value' => isset($resultado['equipo_1']) ? $resultado['equipo_1'] : '',
      '#required' => true,
    );
    $form['field_group'][$resultado['id_partido']]['resultado_1'] = array(
      '#type' => 'textfield',
      '#title' => 'Resultado 1',
      '#size' => 4,
      '#maxlength' => 2,
      '#default_value' => isset($resultado['resultado_1']) ? $resultado['resultado_1'] : '',
    );
    $form['field_group'][$resultado['id_partido']]['equipo_2'] = array(
      '#type' => 'textfield',
      '#title' => 'Equipo 2',
      '#default_value' => isset($resultado['equipo_2']) ? $resultado['equipo_2'] : '',
      '#required' => true,
    );
    $form['field_group'][$resultado['id_partido']]['resultado_2'] = array(
      '#type' => 'textfield',
      '#title' => 'Resultado 2',
      '#size' => 4,
      '#maxlength' => 2,
      '#default_value' => isset($resultado['resultado_2']) ? $resultado['resultado_2'] : '',
    );
  }

  $form['acciones'] = array(
    '#type' => 'actions'
  );

  $form['acciones']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Confirmar',
  );

  return $form;
}

function zuruporra_admin_edit_form_submit($form, &$form_state) {
  $valores = $form_state['values'];

  foreach ($valores['field_group'] as $idPartido => $datos) {
    $dbUpdate = db_update('porra_partidos');
    $dbUpdate->fields(
      array(
        'fecha_partido' => DateTime::createFromFormat('Y-m-d H:i', $datos['fecha_partido'])->getTimestamp(),
        'equipo_1' => $datos['equipo_1'],
        'resultado_1' => strlen($datos['resultado_1']) == 0 ? null : intval($datos['resultado_1']),
        'equipo_2' => $datos['equipo_2'],
        'resultado_2' => strlen($datos['resultado_2']) == 0 ? null : intval($datos['resultado_2'])
      )
    )
      ->condition('id_partido', $idPartido);
    $dbUpdate->execute();

    // Si los partidos tienen el resultado puesto calculamos los puntos normalmente, en caso contrario
    // ponemos los puntos a cero
    if (strlen($datos['resultado_1']) != 0 && strlen($datos['resultado_2']) != 0) {
      calculate_points($idPartido, $form_state['id_jornada']);
    } else {
      calculate_points($idPartido, $form_state['id_jornada'], true);
    }
  }

  drupal_set_message('Modificados los datos de los partidos.');
}

function calculate_points($idPartido, $idJornada, $resetPointZero = false) {
  $selectPartidosJornada = db_select('porra_partidos', 'p');
  $selectPartidosJornada->join('porra_partidos_usuario', 'pp', 'p.id_partido = pp.id_partido');
  $selectPartidosJornada
    ->fields('p', array('id_partido', 'fecha_partido', 'resultado_1', 'resultado_2'))
    ->fields('pp', array('uid', 'fecha_respuesta', 'resultado_1', 'resultado_2', 'riesgo', 'puntos'))
    ->condition('p.id_partido', $idPartido, '=');
  $result = $selectPartidosJornada->execute();

  while ($resultado = $result->fetchAssoc()) {
    $resultadoReal1 = $resultado['resultado_1'];
    $resultadoReal2 = $resultado['resultado_2'];
    $resultadoUser1 = $resultado['pp_resultado_1'];
    $resultadoUser2 = $resultado['pp_resultado_2'];
    $riesgo = ($resultado['riesgo'] == 1);
    $fechaPartido = $resultado['fecha_partido'];
    $fechaRespuesta = $resultado['fecha_respuesta'];

    // Calculamos los puntos por el resultado
    $puntos = 0;

    if ($resultadoReal1 == $resultadoUser1 && $resultadoReal2 == $resultadoUser2) {
      $puntos = 5;
    } elseif (($resultadoReal1 - $resultadoReal2) == ($resultadoUser1 - $resultadoUser2)) {
      $puntos = 3;
    } elseif (($resultadoReal1 - $resultadoReal2) * ($resultadoUser1 - $resultadoUser2) > 0
      || ($resultadoReal1 - $resultadoReal2 == 0 && $resultadoUser1 - $resultadoUser2 == 0)) {

      if ($resultadoReal1 == $resultadoUser1 || $resultadoReal2 == $resultadoUser2) {
        $puntos = 2;
      } else {
        $puntos = 1;
      }
    }

    // Si los puntos son mayores que hemos acertado la victoria
    $acertado = $puntos > 0 ? 1 : 0;

    // Correcciones a los puntos
    if ($riesgo) {
      $puntos = ($resultadoReal1 == $resultadoUser1 && $resultadoReal2 == $resultadoUser2) ? 10 : -1;
    }

    if ($fechaRespuesta > $fechaPartido) {
      $puntos = 0;
    }

    if ($resetPointZero) {
      $puntos = 0;
      $acertado = null;
    }

    // Puntos extra por jornada
    if ($idJornada < 4) {
      $puntos *= 1;
    } elseif ($idJornada == 4) {
      $puntos *= 2;
    } elseif ($idJornada == 5) {
      $puntos *= 3;
    } elseif ($idJornada == 6) {
      $puntos *= 4;
    }

    $dbUpdate = db_update('porra_partidos_usuario');
    $dbUpdate->fields(
      array(
        'puntos' => $puntos,
        'acertado' => $acertado
      )
    )
      ->condition('id_partido', $idPartido)
      ->condition('uid', $resultado['uid']);
    $dbUpdate->execute();
  }
}

function zuruporra_admin_jornadas($form, &$form_state) {
  $form['activacion_jornada'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activar jornadas en el Front'),
  );

  for ($i = 1; $i < 7; $i++) {
    if ($i < 4) {
      $nombre = 'Jornada ' .$i;
    } elseif ($i == 4) {
      $nombre = 'Cuartos';
    } elseif ($i == 5) {
      $nombre = 'Semis';
    } else {
      $nombre = 'La final';
    }

    $form['activacion_jornada']['porra_jornada_'.$i] = array(
      '#type' => 'checkbox',
      '#title' => $nombre,
      '#default_value' => variable_get('porra_jornada_'.$i, 0)
    );
  }

  $usuario = user_uid_optional_load();

  $form['otros'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activar jornadas en el Front'),
    '#access' => $usuario->uid == 1
  );

  $form['otros']['ver_todos_resultados'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ver los resultados de todos los jugadores'),
    '#default_value' => variable_get('porra_ver_todos_resultados', 1),
    '#access' => $usuario->uid == 1
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Modificar'
  );

  return $form;
}

function zuruporra_admin_jornadas_submit($form, &$form_state) {
  for ($i = 1; $i < 7; $i++) {
    variable_set('porra_jornada_' . $i, $form_state['values']['porra_jornada_'.$i]);
  }

  if (isset($form_state['values']['ver_todos_resultados'])) {
    variable_set('porra_ver_todos_resultados',$form_state['values']['ver_todos_resultados']);
  }

  drupal_set_message('Modificación realizada.');
}

function recuperar_nombre_jornada($idJornada) {
  $resultado =
    db_select('porra_jornada', 'j')
      ->fields('j',array('nombre_jornada'))
      ->condition('j.id_jornada', $idJornada, '=')
      ->execute();

  $res = $resultado->fetchAssoc();

  return t($res['nombre_jornada']);
}