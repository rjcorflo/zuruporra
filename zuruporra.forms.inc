<?php
/**
 * Created by PhpStorm.
 * User: RJ Corchero
 * Date: 02/06/2016
 * Time: 17:53
 */

function zuruporra_edit_form($form, &$form_state, $idJornada) {
  $usuario = user_uid_optional_load();

  $form_state['id_jornada'] = $idJornada;

  $selectPartidosJornada = db_select('porra_partidos', 'p');
  $selectPartidosJornada->join('porra_partidos_usuario', 'pp', 'p.id_partido = pp.id_partido');
  $selectPartidosJornada
    ->fields('p', array('id_partido','fecha_partido', 'equipo_1', 'equipo_2'))
    ->fields('pp', array('fecha_respuesta', 'resultado_1', 'resultado_2', 'riesgo', 'puntos'))
    ->condition('p.id_jornada', $idJornada, '=')
    ->condition('pp.uid', $usuario->uid, '=');
  $result = $selectPartidosJornada->orderBy('p.id_partido')->execute();

  $form['field_group'] = array(
    '#type' => 'fieldset',
    '#title' => 'Modificar resultados de mis partidos de esta jornada',
    '#tree' => true,
    '#attributes' => array('class' => array('zuruporra')),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  if ($result->rowCount() > 0) {
    $form_state['tipo'] = 'update';

    while ($resultado = $result->fetchAssoc()) {
      $partidoEmpezado = REQUEST_TIME > $resultado['fecha_partido'];

      $form['field_group']['partidos'][$resultado['id_partido']] = array(
        '#type' => 'fieldset',
        '#tree' => true,
        '#disabled' => $partidoEmpezado
      );

      $form['field_group']['partidos'][$resultado['id_partido']]['resultado_1'] = array(
        '#type' => 'textfield',
        '#title' => $resultado['equipo_1'],
        '#size' => 4,
        '#maxlength' => 2,
        '#default_value' => isset($resultado['resultado_1']) ? $resultado['resultado_1'] : 0,
        '#required' => true,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['resultado_2'] = array(
        '#type' => 'textfield',
        '#title' => $resultado['equipo_2'],
        '#size' => 4,
        '#maxlength' => 2,
        '#default_value' => isset($resultado['resultado_2']) ? $resultado['resultado_2'] : 0,
        '#required' => true,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['riesgo'] = array(
        '#type' => 'checkbox',
        '#title' => '¿Riesgo?',
        '#default_value' => isset($resultado['riesgo']) && $resultado['riesgo'] == 1 ? true : false,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['partido_empezado'] = array(
        '#type' => 'value',
        '#value' => $partidoEmpezado
      );
    }
  } else {
    $form_state['tipo'] = 'insert';

    $selectPartidosJornada = db_select('porra_partidos', 'p');
    $selectPartidosJornada
      ->fields('p', array('id_partido', 'fecha_partido', 'equipo_1', 'equipo_2'))
      ->condition('p.id_jornada', $idJornada, '=');
    $result = $selectPartidosJornada->orderBy('p.id_partido')->execute();

    while ($resultado = $result->fetchAssoc()) {
      $partidoEmpezado = REQUEST_TIME > $resultado['fecha_partido'];

      $form['field_group']['partidos'][$resultado['id_partido']] = array(
        '#type' => 'fieldset',
        '#tree' => true,
      );

      $form['field_group']['partidos'][$resultado['id_partido']]['resultado_1'] = array(
        '#type' => 'textfield',
        '#title' => $resultado['equipo_1'],
        '#size' => 4,
        '#maxlength' => 2,
        '#default_value' => isset($resultado['resultado_1']) ? $resultado['resultado_1'] : 0,
        '#required' => true,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['resultado_2'] = array(
        '#type' => 'textfield',
        '#title' => $resultado['equipo_2'],
        '#size' => 4,
        '#maxlength' => 2,
        '#default_value' => isset($resultado['resultado_2']) ? $resultado['resultado_2'] : 0,
        '#required' => true,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['riesgo'] = array(
        '#type' => 'checkbox',
        '#title' => '¿Riesgo?',
        '#default_value' => isset($resultado['riesgo']) && $resultado['riesgo'] == 1 ? true : false,
        '#disabled' => $partidoEmpezado
      );
      $form['field_group']['partidos'][$resultado['id_partido']]['partido_empezado'] = array(
        '#type' => 'value',
        '#value' => $partidoEmpezado
      );
    }
  }

  $form['field_group']['acciones'] = array(
    '#type' => 'actions'
  );

  $form['field_group']['acciones']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Confirmar',
  );

  return $form;
}

function zuruporra_edit_form_submit($form, &$form_state) {
  $usuario = user_uid_optional_load();

  $valores = $form_state['values']['field_group'];

  if ($form_state['tipo'] == 'insert') {
    $dbInsert = db_insert('porra_partidos_usuario')
      ->fields(array('id_partido', 'uid', 'fecha_respuesta', 'resultado_1', 'resultado_2', 'riesgo'));

    foreach($valores['partidos'] as $idPartido => $datos) {
      if (!$datos['partido_empezado']) {
        $dbInsert->values(array($idPartido, $usuario->uid,REQUEST_TIME, $datos['resultado_1'], $datos['resultado_2'], $datos['riesgo']));
      }
    }

    $dbInsert->execute();

  } elseif ($form_state['tipo'] == 'update') {
    foreach($valores['partidos'] as $idPartido => $datos) {
      if (!$datos['partido_empezado']) {
        $dbUpdate = db_update('porra_partidos_usuario');
        $dbUpdate->fields(
          array(
            'fecha_respuesta' => REQUEST_TIME,
            'resultado_1' => $datos['resultado_1'],
            'resultado_2' => $datos['resultado_2'],
            'riesgo' => $datos['riesgo']
          )
        )
          ->condition('uid', $usuario->uid)
          ->condition('id_partido', $idPartido);
        $dbUpdate->execute();
      }
    }
  }

  drupal_set_message('Modificados los datos de los partidos.');
}