<?php
/**
 * Created by PhpStorm.
 * User: RJ Corchero
 * Date: 02/06/2016
 * Time: 17:53
 */

module_load_include('inc', 'zuruporra', 'zuruporra.forms');

function zuruporra_page($idJornada) {
  $usuarioActual = user_uid_optional_load();


  // Creamos la tabla con los datos de resumen de la partida
  $headers = array(
    array('data' => t('Partido')),
    array('data' => t('Fecha')),
    array('data' => t('Resultado')),
  );

  $rows = array();

  // Partidos de la jornada
  $selectPartidosJornada = db_select('porra_partidos', 'p');
  $selectPartidosJornada
      ->fields('p', array('id_partido', 'grupo', 'fecha_partido','equipo_1', 'equipo_2', 'resultado_1', 'resultado_2'))
      ->condition('p.id_jornada', $idJornada, '=')
      ->orderBy('grupo', 'ASC')
      ->orderBy('id_partido', 'ASC');

  $ejecucion = $selectPartidosJornada->execute();

  while ($datosPartido = $ejecucion->fetchAssoc()) {
    $partidoTexto = '<b>' . $datosPartido['equipo_1'] . '</b> vs <b>' . $datosPartido['equipo_2'] . '</b>';

    $fechaActual = new DateTime();
    $partidoFecha = (new DateTime())->setTimestamp($datosPartido['fecha_partido']);
    $partidoFechaFin =  (new DateTime())->setTimestamp($datosPartido['fecha_partido'])->modify('+90 minute');

    if (!isset($datosPartido['resultado_1']) || !isset($datosPartido['resultado_2'])) {
      if ($fechaActual->getTimestamp() < $partidoFecha->getTimestamp()) {
        $partidoResultado = 'Por jugar';
      } elseif ($fechaActual->getTimestamp() > $partidoFecha->getTimestamp()
          && $fechaActual->getTimestamp() < $partidoFechaFin->getTimestamp()) {
        $partidoResultado = 'Partido jugándose';
      } else {
        $partidoResultado = 'Pendiente de poner resultado';
      }
    } else {
      $partidoResultado = $datosPartido['resultado_1'] . ' - ' . $datosPartido['resultado_2'];
    }

    $rows[$datosPartido['id_partido']] = array(
      'data' => array(
        $partidoTexto,
        $partidoFecha->format('d-m-Y H:i'),
        $partidoResultado
      )
    );

    // Resultado de usuarios por partido
    $selectUsuarioPartido = db_select('porra_partidos_usuario', 'p');
    $selectUsuarioPartido
        ->fields('p', array('id_partido', 'uid', 'fecha_respuesta', 'resultado_1', 'resultado_2', 'riesgo', 'puntos', 'acertado'))
        ->condition('p.id_partido', $datosPartido['id_partido'], '=')
        ->orderBy('uid', 'ASC');
    $ejecucionQueryUsuarioPartido = $selectUsuarioPartido->execute();

    while ($datosUsuarioPartido = $ejecucionQueryUsuarioPartido->fetchAssoc()) {
      $usuarioPartido = user_uid_optional_load($datosUsuarioPartido['uid']);

      if (!isset($headers[$usuarioPartido->name])) {
        if (!empty($usuarioPartido->field_nombre)) {
          $nombreUsuario = field_view_field('user', $usuarioPartido, 'field_nombre', array('label' => 'hidden'));
        } else {
          $nombreUsuario = $usuarioPartido->name;
        }

        $headers[$usuarioPartido->name] = array('data' => $nombreUsuario, 'class' => array('usuario-cabecera'));
      }

      // Resultados solo visibles si se puede ver todos los resultados o si el partido ya haya empezado
      if (variable_get('porra_ver_todos_resultados', 1) == 0
        && $datosPartido['fecha_partido'] > (new DateTime())->getTimestamp()
        && $usuarioPartido->uid != $usuarioActual->uid) {
        $resultadoJugador = '<div class="resultado">-<b>Oculto</b>-</div>';
      } else {
        $resultadoJugador = '<div class="resultado">' . $datosUsuarioPartido['resultado_1'] . ' - ' . $datosUsuarioPartido['resultado_2'];

        if ($datosUsuarioPartido['riesgo'] == 1) {
          $resultadoJugador .= ' (*)';
        }

        $resultadoJugador .= '</div><div class="puntos">Puntos: <span class="puntos-dato">' . $datosUsuarioPartido['puntos'] . '</span></div>';

      }

      $rows[$datosPartido['id_partido']]['data'][$usuarioPartido->name] = array('data' => $resultadoJugador);

      if (isset($datosUsuarioPartido['acertado'])) {
        $class = $datosUsuarioPartido['acertado'] == 1 ? 'acertado' : 'no-acertado';
        $rows[$datosPartido['id_partido']]['data'][$usuarioPartido->name]['class'] = array($class);
      }
    }
  }

  $formulario = drupal_get_form('zuruporra_edit_form', $idJornada);

  $renderResultadosJornada = zuruporra_resultados_jornada($idJornada);
  $renderResultadosGenerales = zuruporra_resultados_generales();

  $renderArray =
    array(
      array(
        '#type' => 'container',
        '#attributes' => array('class' => 'zona-resultados'),
        'resultados_jornada' =>
          array(
            '#theme' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#empty' => t('There is no data for this game.'),
            '#caption' => t('<h3>Resultados de la jornada</h3>'),
            '#attributes' => array('class' => array('zuruporra-tabla')),
            '#sticky' => false
          ),
      ),
      $formulario,
      $renderResultadosJornada,
      $renderResultadosGenerales,
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'zuruporra') . '/css/my.css'),
      ),
    );

  return $renderArray;
}

function zuruporra_resultados_generales() {
  // Creamos la tabla con los datos de resumen de la partida
  $headers = array(
    array('data' => t('Posicion')),
    array('data' => t('Usuario')),
    array('data' => t('Puntos Actuales')),
  );

  $rows = array();

  $selectPartidosJornada = db_select('porra_partidos_usuario', 'p');
  $selectPartidosJornada
    ->fields('p', array('uid'))
    ->addExpression('SUM(puntos)', 'total_puntos');
  $selectPartidosJornada->groupBy('uid')
    ->orderBy('total_puntos', 'DESC');

  $ejecucion = $selectPartidosJornada->execute();

  $totalResultados = $ejecucion->rowCount();

  $posicion = 1;
  while ($resultado = $ejecucion->fetchAssoc()) {
    if ($posicion < 4) {
      $class = 'lideres';
    } elseif ($posicion > $totalResultados - 3) {
      $class = 'ridiculos';
    } else {
      $class = '';
    }

    $usuario = user_uid_optional_load($resultado['uid']);
    if (!empty($usuario->field_nombre)) {
      $nombreUsuario = field_view_field('user', $usuario, 'field_nombre', array('label' => 'hidden'));
    } else {
      $nombreUsuario = $usuario->name;
    }

    $rows[] = array(
      'data' => array(
        $posicion,
        array('data' => $nombreUsuario),
        $resultado['total_puntos'],
      ),
      'class' => array($class)
    );

    $posicion++;
  }

  $renderArray =
    array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('There is no data for this game.'),
      '#caption' => t('<h3>Clasificación General</h3>'),
      '#attributes' => array('class' => array('zuruporra-tabla'))
    );

  return $renderArray;
}

function zuruporra_resultados_jornada($idJornada) {
  // Creamos la tabla con los datos de resumen de la partida
  $headers = array(
    array('data' => t('Posicion')),
    array('data' => t('Usuario')),
    array('data' => t('Puntos Actuales')),
  );

  $rows = array();

  $selectPartidosJornada = db_select('porra_partidos', 'p');
  $selectPartidosJornada->join('porra_partidos_usuario', 'pp', 'p.id_partido = pp.id_partido');
  $selectPartidosJornada
    ->fields('pp', array('uid'))
    ->condition('p.id_jornada', $idJornada, '=')
    ->addExpression('SUM(puntos)', 'total_puntos');
  $selectPartidosJornada->groupBy('uid')
    ->orderBy('total_puntos', 'DESC');

  $ejecucion = $selectPartidosJornada->execute();

  $totalResultados = $ejecucion->rowCount();
  $posicion = 1;
  while ($resultado = $ejecucion->fetchAssoc()) {
    if ($posicion < 4) {
      $class = 'lideres';
    } elseif ($posicion > $totalResultados - 3) {
      $class = 'ridiculos';
    } else {
      $class = '';
    }

    $usuario = user_uid_optional_load($resultado['uid']);
    if (!empty($usuario->field_nombre)) {
      $nombreUsuario = field_view_field('user', $usuario, 'field_nombre', array('label' => 'hidden'));
    } else {
      $nombreUsuario = $usuario->name;
    }

    $rows[] = array(
      'data' => array(
        $posicion,
        array('data' => $nombreUsuario),
        $resultado['total_puntos'],
      ),
      'class' => array($class)
    );

    $posicion++;
  }

  $renderArray =
    array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('There is no data for this game.'),
      '#caption' => t('<h3>Clasificación de esta Jornada</h3>'),
      '#attributes' => array('class' => array('zuruporra-tabla'))
    );

  return $renderArray;
}